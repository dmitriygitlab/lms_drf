#!/bin/bash

# Установка Python и Poetry
curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py | python3 -
source $HOME/.poetry/env

# Установка зависимостей проекта
poetry install --no-interaction --no-ansi

# Активация виртуального окружения
source "$(poetry env info --path)/bin/activate"

# Миграции базы данных
python manage.py migrate

# Сборка статических файлов
python manage.py collectstatic --no-input

# Деактивация виртуального окружения
deactivate


#python3 -m venv env
#source env/bin/activate
#pip3 install -r requirements.txt
#python3 manage.py migrate
#pethon3 manage.py collectstatic --no-input
#deactivate